import React from 'react';
import '../style/style.css';

class Questions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            questions: [],
            addQuestion: false,
            currentQuestion: '',
            addNewVariation: false,
            currentVariation: '',
            currentIndex: '',
            editQuestion: false
        }
    }

    openAddNewQuestionInput(e) {
        this.setState({
            addQuestion: true
        })
    }



    handleChange(e) {
        this.setState({
            currentQuestion: e.target.value,
        })
    }

    handleAddNewQuestion(e, index) {
        var temp = {
            'question': this.state.currentQuestion,
            'id': Math.random(),
            'currentVariation': ''
        }
        this.state.questions.push(temp);
        this.setState({
            addQuestion: false
        })
    }

    handleVariationChange(e, index) {
        var temp = this.state.questions;
        temp[index].currentVariation = e.target.value;
        this.setState({
            questions: temp
        })
    }

    handleAddVariation(e, index) {

        var temp = this.state.questions;
        temp[index].variations = temp[index].variations;
        if (temp[index].variations == null) {
            temp[index].variations = []
        }
        var check = { 'variation': temp[index].currentVariation }
        temp[index].variations.push(check);
        this.setState({
            questions: temp,
            addNewVariation: false
        }, () => {
            var temp1 = this.state.questions;
            temp1[index].currentVariation = '';
            this.setState({
                questions: temp1
            })
        })
    }

    handleDeleteVariation(e, index, index1) {
        var temp = this.state.questions;
        temp[index].variations.splice(index1, 1);
        this.setState({
            questions: temp
        })
    }

    deleteQuestion(e, index) {
        var temp = this.state.questions;
        temp.splice(index, 1);
        this.setState({
            questions: temp
        })
    }

    editQuestionModal(index) {
        this.setState({
            editQuestion: true
        })
    }

    handleEditQuestion(e, index) {
        var temp = this.state.questions;
        temp[index].question = e.target.innerText;
        this.setState({
            questions: temp
        })
    }

    handleEditvariationChange(e, index, index1) {
        var temp = this.state.questions;
        temp[index].variations[index1].variation = e.target.innerText;
        this.setState({
            questions: temp
        })
    }

    handleVariationClick(e, index, index1) {
        var temp = this.state.questions;
        this.setState({
            questions: temp
        })
    }

    render() {
        return (
            <div>

                <div>
                    <div className="header">
                        <div className="headerInnerDiv">
                            <img src="/images/logo.png" className="headerImage" />
                            <h3 className="headerAdmin">Admin Dashboard</h3>
                            <h3 className="headerName">Sahil Gupta</h3>
                        </div>
                    </div >

                    <div className="innerFaqHeader">
                        <h3>FAQ Builder</h3>
                    </div >

                    <div className="questionsLabelDiv">
                        <label className="questionsHeaderLabel">Questions</label>
                    </div>

                    <div className="parentDiv">

                        {this.state.questions.length > 0 &&
                            <div>
                                {this.state.questions.map((item, index) => (
                                    <div key={index}>
                                        <div>
                                            <h3 className='showQuestion' contentEditable="true" suppressContentEditableWarning={true} onBlur={(e) => this.handleEditQuestion(e, index)}>{item.question}</h3>
                                            <div className="deleteQuestionButtonDiv">
                                                <button onClick={(e) => this.deleteQuestion(e, index)}>
                                                    <img src="/images/delete.svg" />
                                                </button>
                                            </div>
                                        </div>

                                        <div className='afterQuestionDiv'>
                                            {item.variations && item.variations.length > 0 && <h4 style={{ fontWeight: '1000' }} className='variationLabel'>Variations:</h4>}
                                            {item.variations &&
                                                <div>
                                                    <ul className="listStyleType list-group">
                                                        {item.variations.map((item1, index1) => (
                                                            <li className='showVariation' key={index1} >
                                                                <span contentEditable="true" suppressContentEditableWarning={true} onClick={(e) => this.handleVariationClick(e, index, index1)} onBlur={(e) => this.handleEditvariationChange(e, index, index1)}>{item1.variation}</span>
                                                                <button className='deleteVariationButton badge' onClick={(e) => this.handleDeleteVariation(e, index, index1)}><img src="/images/delete.svg" /></button>
                                                            </li>
                                                        ))}
                                                    </ul>
                                                </div>
                                            }

                                        </div>

                                        <div className='inputVariationDiv'>
                                            <input placeholder='+ Add New Variation' className='addVariationInputBox' type='text' name='variation' value={item.currentVariation} onChange={(e) => this.handleVariationChange(e, index)} />
                                            <button disabled={!item.currentVariation} className="innerAddVariationButton" onClick={(e) => this.handleAddVariation(e, index)}>Add</button>
                                        </div>
                                        <div className='hrStyle'>
                                            <hr />
                                        </div>
                                    </div>
                                ))}
                            </div>
                        }

                        <div>
                            {this.state.addQuestion &&
                                <div>
                                    <input placeholder='+ Add New Question' className="innerAddQuestionInput" name='question' type='text' onChange={(e) => this.handleChange(e)} />
                                    <button disabled={!this.state.currentQuestion} className="innerAddQuestionButton" onClick={(e) => this.handleAddNewQuestion(e)}>Add</button>
                                </div>
                            }
                        </div>

                        <div className="addQuestionButtonDiv">
                            <button className='addNewQuestionButton' onClick={(e) => this.openAddNewQuestionInput(e)}>
                                <img className='addNewQuestionImage' src="/images/add.svg" />
                                <span className='addNewQuestionSpan'>Add New Question</span></button>
                        </div>
                    </div>
                </div >
            </div>
        )
    }
}

export default Questions;