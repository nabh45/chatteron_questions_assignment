import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Questions from './components/questions';

export default () => (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Questions} />
    </Switch>
  </BrowserRouter>
)